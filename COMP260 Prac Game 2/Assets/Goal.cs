﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {
	public AudioClip ScoreClip;
	private AudioSource audio;
	public Transform startingPos;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		
	}

	void OnTriggerEnter(Collider collider){
		audio.PlayOneShot (ScoreClip);
		PuckControl puck =
			collider.gameObject.GetComponent<PuckControl> ();
		puck.ResetPosition ();
	}

	
	// Update is called once per frame
	void Update () {
		audio.PlayOneShot (ScoreClip);
	}
}
