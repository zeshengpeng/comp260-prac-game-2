﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]

public class PuckControl : MonoBehaviour {
	public AudioClip wallCollideClip;
	private AudioSource audio;
	public LayerMask paddleLayer;
	public AudioClip paddleCollideClip;


	// Use this for initialization
	void OnCollisionEnter(Collision collision){
		if (paddleLayer.Contains (collision.gameObject)) {
			audio.PlayOneShot (paddleCollideClip);
		} else {
			audio.PlayOneShot (wallCollideClip);
		}
	}
	void OnCollisionStay(Collision collision){
		Debug.Log ("Collision Stay" + collision.gameObject.name);
	}
	void OnCollisionExit(Collision collision){
		Debug.Log ("Collision Enter" + collision.gameObject.name);
	}
	void Start () {
		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody> ();
		ResetPosition ();
	}
	public void ResetPosition() {
		rigidbody.MovePosition (startingPos.position);
		rigidbody.velocity = Vector3.zero;
	}
	// Update is called once per frame
	void Update () {
	
	}
}
