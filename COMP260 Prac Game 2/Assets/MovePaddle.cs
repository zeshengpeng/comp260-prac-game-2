﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
	private Rigidbody rigidbody;
	public float speed = 20f;
	public float force = 10f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}
	private Vector3 GetMousePosition(){
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Plane plane = new Plane (Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);
	
	}
	void onDrawGizmos(){
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}
	void FixedUpdate() {
		Vector3 pos = GetMousePosition ();
		Vector3 dir = pos - rigidbody.position;
		rigidbody.AddForce (dir.normalized * force);
	}
}
